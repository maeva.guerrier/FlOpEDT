from django.db import models
from enum import Enum

# Create your models here.

class VoteType(Enum):
	EDT = "EDT"
	QUOTE = "QUOTE"

class Opinion(Enum):
	LIKE = "l"
	DISLIKE ="d"

class Votes(models.Model):
	student = models.ForeignKey('people.Student', on_delete = models.CASCADE, blank=False, null=False)
	edt_version = models.ForeignKey('base.EdtVersion', on_delete = models.CASCADE, blank=False, null=False)
	comment = models.CharField(max_length=100,
                                   verbose_name="Commentaire ?")
	type_vote = models.CharField(max_length=5, default=VoteType.EDT, choices=[(v,v.value) for v in VoteType])
	opinion = models.CharField(max_length=1, default=Opinion.LIKE, choices=[(o,o.value) for o in Opinion])

