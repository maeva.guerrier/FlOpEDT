# Frameworks et Logiciels Opensources avec Python

---

## Sujet : Compter des votes pour l'EDT


    Composition du groupe :
    
        |> GUERRIER Maëva
        |> MORA Michaël
        |> MERAT Mélissa
        |> SERIEYS Axel
        |> EVRARD Damien
        |> LAURENT Anthony
        |> LIRAUD Lionel
        
---

## Objectifs du projet :


Notre projet consiste en l'ajout d'un système de vote à l'EDT existant. Ce système de vote devra permettre aux utilisateurs connectés de manifester leur satisfaction ou insatifaction concernant l'EDT de la semaine courante et la citation apparaissant en dessous. Ces votes auront une conséquence sur les futures générations de l'EDT.

---
---

![Logo](./FlOpEDT/base/static/base/img/flop2.png)